#include "spsh.h"

/* Loop through reading commands until we see an EOF (^D) */
void loop(void) {
    char *line;

    /* Loop until we break */
    while (1) {

        /* Read a line from the user */
        line = readline(PROMPT);

        /* If the line is valid, try to run it as a command */
        if (line != NULL) {
            if (strlen(line) > 0) {
                cmd(line);
            }
        }

        /* If the line is EOF (^D), break out of the loop */
        else {
            fputs("\n", stdout);
            break;
        }
    }

    return;
}
