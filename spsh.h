/* Libraries I need */
#include <readline/history.h>
#include <readline/readline.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

/* A welcome and a warning */
#define WELCOME "Welcome to spsh: the shitposting shell"
#define WARNING "(pretty much nothing works, just fyi)"

/* Maximum number of arguments and environment variables */
#define MAX_ARGS 64

/* The prompt to use */
#define PROMPT "spsh$ "

/* The token used to separate arguments */
#define ARG_DELIM " "

/* Environment variables for this script */
extern char **environ;

/* Function prototypes to soothe separate files */
int banner(void);
void loop(void);
void cmd(char *line);
