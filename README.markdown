spsh: the shitposting shell
===========================

An experimental Bourne-style shell for me to learn moar C. Don't actually use
this for any serious reason.

Installing
----------

    $ sudo apt-get install build-essential libreadline6-dev
    $ make

Working
-------

*   Readline keys
*   Commands (with fully-qualified paths) and arguments
*   ^D to exit

Not working but planned
-----------------------

In rough order of priority:

*   At least a few simple builtins, like "cd" and "exit"
*   Prompt with username and current directory
*   Environment variables
*   Quoting/escaping arguments (i.e. something better than just strtok())
*   Command history

License
-------

Copyright (c) [Tom Ryder][1]. Distributed under [GPLv3][2].

[1]: https://sanctum.geek.nz/
[2]: https://www.gnu.org/licenses/gpl-3.0.en.html
