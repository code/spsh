#include "spsh.h"

/* Print the welcome and warning banners */
int banner(void) {
    return fprintf(stdout, "%s\n%s\n", WELCOME, WARNING);
}
