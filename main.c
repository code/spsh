#include "spsh.h"

/* Entry function */
int main(void) {

    /* Show the banner with the warning */
    if (banner() < 0) {
        fputs("Couldn't write welcome banner to stdout!", stderr);
        exit(EXIT_FAILURE);
    }

    /* Start looping through commands */
    loop();

    /* If we get to this point, things should be good */
    exit(EXIT_SUCCESS);
}
