.POSIX:
.PHONY: all clean install

CC = clang
LDFLAGS = -lreadline
CFLAGS = -std=c90 -Weverything
PREFIX = /usr/local

all: spsh

spsh: main.o banner.o cmd.o loop.o
	$(CC) $(CFLAGS) main.o banner.o cmd.o loop.o $(LDFLAGS) -o spsh

tags:
	ctags -- *.c *.h

clean:
	rm -f -- *.o spsh tags

install: spsh
	install -m 0755 spsh $(PREFIX)/bin
